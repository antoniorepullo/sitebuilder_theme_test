const gulp = require('gulp');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const importer = require('node-sass-globbing');
const autoprefixer = require('gulp-autoprefixer');
const stripCssComments = require('gulp-strip-css-comments');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const livereload = require('gulp-livereload');

let sass_config = {
  importer: importer,
  includePaths: [
    'node_modules/breakpoint-sass/stylesheets/',
    'node_modules/singularitygs/stylesheets/',
    'node_modules/modularscale-sass/stylesheets',
    'node_modules/compass-mixins/lib/'
  ]
};

// Gulp plumber error handler
let onError = function(err) {
  console.log(err);
}


function sassThemes(){
  return gulp.src('sass/*.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass(sass_config).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(stripCssComments({preserve: false}))
    .pipe(cssmin())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('css'));
}

function watch(){
  // Whenever a stylesheet is changed, recompile
  gulp.watch('sass/**', {usePolling: true}, sassThemes);

  // Create a LiveReload server
  livereload.listen();

  // Watch any file for a change and reload as required
  gulp.watch([
    'css/style.css'
  ], {usePolling: true}).on('change', function(files) {
    livereload.changed(files);
  })
}

exports.compile = sassThemes;
exports.default = gulp.series(sassThemes, watch);
